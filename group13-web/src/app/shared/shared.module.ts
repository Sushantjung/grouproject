import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { MsgService } from './services/msg.service';
import { SidebarComponent } from './sidebar/sidebar.component';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [
    PageNotFoundComponent,
    HeaderComponent, 
    FooterComponent, SidebarComponent],
  imports: [
    CommonModule,
    RouterModule
  ],
  providers:[MsgService],
  exports: [HeaderComponent, FooterComponent, SidebarComponent]
})
export class SharedModule { }
