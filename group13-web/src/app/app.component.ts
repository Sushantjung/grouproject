import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'group13-web';
  loggedIn:boolean=false;
  constructor(){
    if(localStorage.getItem('token')){

     this.loggedIn = true;

    }
  }

  isloggedIn(){


    if(localStorage.getItem('token')){
      return true;
    }else{
      return false;
    }
  }

  }

