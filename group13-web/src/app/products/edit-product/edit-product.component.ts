import { Component, OnInit } from '@angular/core';
import { ProductService } from '../services/product.service';
import { MsgService } from 'src/app/shared/services/msg.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css']
})
export class EditProductComponent implements OnInit {


  productId;
  product;
  loading:boolean=false;
  //submitting:boolean=false;

  constructor(
    public productService:ProductService,
    public msgService:MsgService,
    public router:Router,
    public activeRouter:ActivatedRoute
    ) { 

    }

  ngOnInit() {
    this.loading=true;

    this.productId=this.activeRouter.snapshot.params['id'];
    this.productService.getById(this.productId)
    .subscribe(
      (data:any)=>{
        this.loading=false;
        console.log('data is here>>',data);
        this.product = data;
        this.product.tags=data.tags.join(',');
           },
      error=>{
          this.loading=false;
          this.msgService.showError(error);
          }
      )


  }




  

  submit(){
 //this.submitting=true;
 this.productService.update(this.product._id, this.product)
 .subscribe(  
   data=>{

    this.msgService.showSucess('product updated successfully');
    this.router.navigate(['/product/list']);
   },
   
   error=>{
     //this.submitting=false;
    this.msgService.showError(error);
   }


  )



  }


    
}


