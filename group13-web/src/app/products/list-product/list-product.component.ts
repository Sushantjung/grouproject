import { Component, OnInit } from '@angular/core';
import { MsgService } from 'src/app/shared/services/msg.service';
import { Router } from '@angular/router';
import { ProductService } from '../services/product.service';

@Component({
  selector: 'app-list-product',
  templateUrl: './list-product.component.html',
  styleUrls: ['./list-product.component.css']
})
export class ListProductComponent implements OnInit {
  products;
  constructor(
    public msgService:MsgService,
    public router:Router,
    public productService:ProductService
  ) { }

  ngOnInit() {

    this.productService.get()
    .subscribe(
       data=>{
        console.log('all data of products',data); 
        this.products=data;

       },
       error=>{
       this.msgService.showError(error);
       }


    )
  }

 removeProduct(id,i){

     let removeConfirm= confirm("Are you sure to delete??");
      if(removeConfirm){
   console.log('data removed from database');
   this.productService.remove(id)
       .subscribe(
         (data)=>{
           this.msgService.showSucess('Product Deleted');
           this.products.splice(i,1);
         },
         (error)=>{
          this.msgService.showError(error);
         }
         
       )
     }

  }


}
