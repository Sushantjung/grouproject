import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Product } from '../models/product.model';


@Injectable()

export class ProductService{
    url:string;

    constructor(public http:HttpClient){

       this.url=environment.Base_URL + 'product';
         
    }
    add(data: Product){

        return this.http.post(this.url, data, this.getOptions());

    }
    get(){
        return this.http.get(this.url, this.getOptions());

    }
    getById(id:string){

        return this.http.get( `${this.url}/${id}`, this.getOptions());


    }


    update(id:string, data:Product){

        //return this.http.put(this.url + '/' + id, data,this.getOptions());
        return this.http.put( `${this.url}/${id}`, data, this.getOptions());

    }

    remove(id){
       // return this.http.delete(this.url + '/' + id, this.getOptions());
       return this.http.delete( `${this.url}/${id}`, this.getOptions());

    }


    search(condition:Product){
        //return this.http.post(this.url + '/search',condition, this.getOptions());
        return this.http.post( `${this.url}/search`,condition, this.getOptions());


    }
    getOptions(){

        let headers= {
               
          headers: new HttpHeaders({
          
          'Content-Type':'application/json',
          'token':localStorage.getItem('token')
          
          
          })
          
          
          }
          return headers;
  
       
  
      }
  



}