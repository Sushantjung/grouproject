import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { MsgService } from 'src/app/shared/services/msg.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
     submitting:boolean=false;
     user;
  constructor(
    public authService:AuthService,
    public msgService:MsgService,
    public router:Router
  ) { 
    this.user={
     username:'',
     password:'',
     name:'',
     email:'',
     address:'',
     phoneNumber:'',
     dob:'',
     gender:''


    }

  }

  ngOnInit() {
  }


  submit(){
  this.submitting=true;
  this.authService.register(this.user)
  .subscribe(

     (data)=>{


      this.msgService.showSucess('registration Successful');
      this.router.navigate(['/auth/login'])
     },
     (error)=>{


      this.msgService.showError(error);
     }


  )


  }

}
