import {Component} from '@angular/core' ;
import { Router }  from '@angular/router';

import { AuthService } from '../services/auth.service';
import {Observable}  from 'rxjs';
import{MsgService} from './../../shared/services/msg.service';
@Component ({

     selector : 'app-login',
     templateUrl: './login.component.html',
     styleUrls: ['./login.component.css']

})

export class LoginComponent{
    user;
    submitting:boolean=false;

    constructor(public router:Router,
                public msgService:MsgService,
                public authService: AuthService,
                ){
     this.user={

           username:'',
           password: ''

     }

    }
     
    askMoney(){


        return new Promise((resolve,reject)=>{

         setInterval(() => {
             console.log('i am called');
             resolve('30000');
         }, 2000);



        })

    
    }

    askNote(){

     return Observable.create((observer)=>{

      setInterval(() => {
          observer.next('data from observable');
      }, 2000);

     })

    }
    
    login(){
        this.msgService.showInfo('login in progress');
        //console.log(this.authService.upper('sushant'));
        this.authService.login(this.user)
        .subscribe(
         (data:any)=>{

          console.log('data from obsevable>>',data);
          this.msgService.showSucess('login Successful');
          this.router.navigate(['/user']);
          //store data in local storage
          localStorage.setItem('token', data.token);

         },
         (err)=>{
             this.msgService.showError(err);
         //console.log('error data from observable>>',err);

         }



        )
        //  .then((data)=>{
        //       console.log('data from promise>>',data);

        //  })
        //  .catch((err)=>{

        //   console.log('error data from promise',err);


        //  })



         
        // this.askMoney()
        // .then((data)=>{
        //  console.log('this.data>>',data);
        // })
        // .catch((err)=>{

        //   console.log('this.error is ',err);

        // })

        // this.askNote().subscribe(

        //  (data)=>{
        //   console.log('success from observable',data);


        //  },
        //  (err)=>{
        //   console.log('faileure from observable',err);

        //  },
        //   (complete)=>{
        //       console.log('no any data');
        //   }
        // )
         
        
        //  this.submitting=true;
        // //alert('i am called');
        // console.log('this.username >>', this.user );
        // this.toastr.success('Login Success');
        // //console.log('this.passowrd>>', this.password);
        // setTimeout(() => {
        //     this.submitting=false;
        //     this.router.navigate(['/user'],{
        //          queryParams:{

        //             name:'ram',add:'mulpani',
        //          }
        //     });

        // this.toastr.info('you must redirect to dashbord');
        // }, 3000);
        
    }
     

}