import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HttpClientModule}  from '@angular/common/http';
 
import { FormsModule } from '@angular/forms';
import { AuthRoutingModule } from './auth.routing';
import { LoginComponent } from './login/login.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { RegisterComponent } from './register/register.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { HomeComponent } from './home/home.component';
import { SharedModule } from '../shared/shared.module';
import { AuthService } from './services/auth.service';
//import { BrowserModule } from '@angular/platform-browser';




@NgModule({
  declarations: [
    LoginComponent,
    RegisterComponent,
    ForgotPasswordComponent,
    ResetPasswordComponent,
    HomeComponent

  ],
  imports: [
   CommonModule,
    FormsModule,
    AuthRoutingModule,
    SharedModule,
    HttpClientModule 
  ],
  providers:[AuthService]
})
export class AuthModule { }
